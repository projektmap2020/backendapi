import json
import struct
from operator import add
from functools import reduce

zapPot = "dataCustom.json"
zaporedje = 0
razlika = 0
bitArr = 0
biti = 0

def split(word): 
    return [char for char in word]  

def zapOpen():
    global zapPot, zaporedje
    zaporedje = []
    zaporedjeCrk = []

    with open(zapPot) as fp:
        for line in fp:
            tmpZaporedje = [ split(item) for item in line.split() ]
            for char in tmpZaporedje:
                zaporedjeCrk.extend(char)

    for char in zaporedjeCrk:
        zaporedje.append(ord(char))

    #print(zaporedje)

def razlika():
    global zaporedje, razlika
    razlika = [0] * len(zaporedje)
    razlika[0] = zaporedje[0]
    for i in range(1, len(zaporedje)):
        razlika[i] = zaporedje[i] - zaporedje[i-1]

def odpriZAP():
    global zapPot
    zapPot = filedialog.askopenfilename(initialdir="/", title="Izberi datoteko", filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
    zapOpen()

def prvaVrednost():
    global bitArr, razlika
    bitArr = []
    biti = format(razlika[0], '08b')
    for b in biti:
        bitArr.append(b)

def zapisiRazliko(st, biti):
    global bitArr
    vmes = '00'
    for b in vmes:
        bitArr.append(b)
    if(biti == '00'):
        vmes = '00'
        for b in vmes:
            bitArr.append(b)
        st = st + 2
        bits = format(st, '02b')
    elif(biti == '01'):
        vmes = '01'
        for b in vmes:
            bitArr.append(b)
        if(st<0):
            st = st + 6
        else:
            st = st + 1
        bits = format(st, '03b')
    elif(biti == '10'):
        vmes = '10'
        for b in vmes:
            bitArr.append(b)
        if (st < 0):
            st = st + 14
        else:
            st = st + 1
        bits = format(st, '04b')
    elif(biti == '11'):
        vmes = '11'
        for b in vmes:
            bitArr.append(b)
        if (st < 0):
            st = st + 30
        else:
            st = st + 1
        bits = format(st, '05b')
    for b in bits:
        bitArr.append(b)

def zapisiPonavljanje(st):
    global bitArr
    vmes = '01'
    for b in vmes:
        bitArr.append(b)
    bits = format(st, '03b')
    for b in bits:
        bitArr.append(b)

def absolutnoKodiranje(st):
    global bitArr
    vmes = '10'
    for b in vmes:
        bitArr.append(b)

    if st > 0:
        bits = format(st, '09b')
    else:
        nova = st - st - st
        bits = format(nova, '08b')
        bitArr.append('1')

    for b in bits:
        bitArr.append(b)

def konec():
    global bitArr
    vmes = '11'
    for b in vmes:
        bitArr.append(b)

def shrani():
    global bitArr

    f = open('comp.bin', 'wb')
    for b in bitArr:
        f.write(int(b).to_bytes(1, 'big'))

def compress():
    global razlika, zaporedje, bitArr
    razlika()
    prvaVrednost()
    i = 1
    while i < len(razlika):
        if (razlika[i] >= -30) and (razlika[i] <= -15) or (razlika[i] <= 30) and (razlika[i] >= 15):
            zapisiRazliko(razlika[i], '11')
        elif (razlika[i] < -30 or razlika[i] > 30):
            absolutnoKodiranje(razlika[i])
        if (razlika[i] >= -14) and (razlika[i] <= -7) or (razlika[i] <= 14) and (razlika[i] >= 7):
            zapisiRazliko(razlika[i], '10')
        if (razlika[i] >= -6) and (razlika[i] <= -3) or (razlika[i] <= 6) and (razlika[i] >= 3):
            zapisiRazliko(razlika[i], '01')
        if (razlika[i] >= -2) and (razlika[i] <= 2):
            if razlika[i] == 0:
                n=1
                while (i+n<len(razlika)) and (razlika[i+n] == 0):
                    n=n+1
                i = i + n
                if n > 8:
                    while n > 8:
                        zapisiPonavljanje(7)
                        n = n - 8
                    if n != 0:
                        zapisiPonavljanje(n-1)
                else:
                    zapisiPonavljanje(n-1)
                i = i - 1
            else:
                zapisiRazliko(razlika[i], '00')
        i = i + 1
    konec()
    shrani()
    print("KONCANO")

zapOpen()
compress()