from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    data = {'a': 7, 'b': 3.14}
    print("podatki bodo poslani")
    req = comm.isend(data, dest=1, tag=11)
    req.wait()
elif rank == 1:
    req = comm.irecv(source=0, tag=11)
    data = req.wait()
    print(data)
    poslano = "Neke neke nekee"
    req1 = comm.isend(poslano, dest=0, tag=12)
    req1.wait()

if rank == 0:
    req1 = comm.irecv(source=1, tag=12)
    poslano = req1.wait()
    print("Sporocilo je bilo prejeto...")
    print(poslano)