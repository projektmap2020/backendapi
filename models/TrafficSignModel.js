let mongoose = require('mongoose');
let Schema   = mongoose.Schema;

let TrafficSignSchema = new Schema({
	'signType' : String,
	'longitude' : Number,
	'latitude' : Number,
	'date' : Date
});

module.exports = mongoose.model('TrafficSign', TrafficSignSchema);
