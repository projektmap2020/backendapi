let mongoose = require('mongoose');
let Schema   = mongoose.Schema;

let RoadConditionSchema = new Schema({
	'condition' : Number,
	'longitude' : Number,
	'latitude' : Number,
	'date' : Date
});

module.exports = mongoose.model('RoadCondition', RoadConditionSchema);
