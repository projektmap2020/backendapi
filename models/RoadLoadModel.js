let mongoose = require('mongoose');
let Schema   = mongoose.Schema;

let RoadLoadSchema = new Schema({
	'roadName' : String,
	'year' : Number,
	'numberOfVehicles' : Number,
	'numberOfCars' : Number,
	'numberOfMotorbikes' : Number,
	'numberOfBuses' : Number,
	'numberOfTrucks' : Number
});

module.exports = mongoose.model('RoadLoad', RoadLoadSchema);
