let RoadLoadModel = require('../models/RoadLoadModel.js');
let spawn = require('child_process').spawn;
var path = require('path');

module.exports = {

    list: function (req, res) {
        RoadLoadModel.find(function (err, RoadLoads) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting RoadLoad.',
                    error: err
                });
            }
            return res.json(RoadLoads);
        });
    },

    show: function (req, res) {
        let id = req.params.id;
        RoadLoadModel.findOne({_id: id}, function (err, RoadLoad) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting RoadLoad.',
                    error: err
                });
            }
            if (!RoadLoad) {
                return res.status(404).json({
                    message: 'No such RoadLoad'
                });
            }
            return res.json(RoadLoad);
        });
    },

    create: function (req, res) {
        req.body.forEach(obj => {
            obj.forEach(element => {
                let RoadLoad = new RoadLoadModel({
                    roadName : element.roadName,
                    year : element.year,
                    numberOfVehicles : element.numberOfVehicles,
                    numberOfCars : element.numberOfCars,
                    numberOfMotorbikes : element.numberOfMotorbikes,
                    numberOfBuses : element.numberOfBuses,
                    numberOfTrucks : element.numberOfTrucks
                });

                RoadLoad.save(function (err, RoadLoad) {
                    if (err) {
                        console.log("Napaka: " + err);
                    }
                    console.log("Vstavljeno: " + RoadLoad);
                });
            });
        });
        res.send("DONE");
    },

    update: function (req, res) {
        let id = req.params.id;
        RoadLoadModel.findOne({_id: id}, function (err, RoadLoad) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting RoadLoad',
                    error: err
                });
            }
            if (!RoadLoad) {
                return res.status(404).json({
                    message: 'No such RoadLoad'
                });
            }

            RoadLoad.roadName = req.body.roadName ? req.body.roadName : RoadLoad.roadName;
			RoadLoad.year = req.body.year ? req.body.year : RoadLoad.year;
			RoadLoad.numberOfCars = req.body.numberOfCars ? req.body.numberOfCars : RoadLoad.numberOfCars;
			RoadLoad.numberOfMotorbikes = req.body.numberOfMotorbikes ? req.body.numberOfMotorbikes : RoadLoad.numberOfMotorbikes;
			RoadLoad.numberOfBuses = req.body.numberOfBuses ? req.body.numberOfBuses : RoadLoad.numberOfBuses;
			RoadLoad.numberOfTrucks = req.body.numberOfTrucks ? req.body.numberOfTrucks : RoadLoad.numberOfTrucks;
			
            RoadLoad.save(function (err, RoadLoad) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating RoadLoad.',
                        error: err
                    });
                }

                return res.json(RoadLoad);
            });
        });
    },

    remove: function (req, res) {
        let id = req.params.id;
        RoadLoadModel.findByIdAndRemove(id, function (err, RoadLoad) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the RoadLoad.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    addTestData: function(req, res) {
        var roadNames = ["Cesta 1", "Cesta 2", "Cesta 3", "Cesta 4", "Avtocesta 1", "Avtocesta 2"];

        roadNames.forEach(roadName => {
            for (var i = 1997; i < 2019; i++) {
                var numberOfCars = Math.floor((Math.random() * 1000) + 1);
                var numberOfMotorbikes = Math.floor((Math.random() * 1000) + 1);
                var numberOfBuses = Math.floor((Math.random() * 1000) + 1);
                var numberOfTrucks = Math.floor((Math.random() * 1000) + 1);
                var numberOfVehicles = numberOfBuses + numberOfCars + numberOfMotorbikes + numberOfTrucks;

                let RoadLoad = new RoadLoadModel({
                    roadName : roadName,
                    year : i,
                    numberOfVehicles : numberOfVehicles,
                    numberOfCars : numberOfCars,
                    numberOfMotorbikes : numberOfMotorbikes,
                    numberOfBuses : numberOfBuses,
                    numberOfTrucks : numberOfTrucks
                });

                RoadLoad.save(function (err, RoadLoad) {
                    if (err) {
                        console.log("Napaka: " + err);
                    }
                    console.log("Uspesno vstavljeno: " + RoadLoad);
                });
            }
        });

        res.send("DONE");
    },

    getDataForRoadAndYear: function(req, res) {
        let roadName = req.params.roadName;
        let year = req.params.year;

        RoadLoadModel.find({roadName: roadName, year: year}, function (err, RoadLoad) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting RoadLoad.',
                    error: err
                });
            }
            if (!RoadLoad) {
                return res.status(404).json({
                    message: 'Ni podatkov za to kombinacijo leta in cestisca'
                });
            }
            return res.json(RoadLoad);
        });
    },

    sendCompressedData: function(req, res) {
        const pyScript = spawn('python3', ['kompresijaServer.py']);

        /*pyScript.stdout.on('data', (data) => {
            if (data == "KONCANO") {
                res.sendFile(__dirname + '/../comp.bin');
            }
        });*/

        pyScript.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
            return res.status(500).send("Error : " + data);
        });

        pyScript.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
            res.sendFile(path.resolve('comp.bin'));
        });
    } 
};
