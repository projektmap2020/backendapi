let TrafficSignModel = require('../models/TrafficSignModel.js');
let spawn = require('child_process').spawn;

module.exports = {

    list: function (req, res) {
        TrafficSignModel.find(function (err, TrafficSigns) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting TrafficSign.',
                    error: err
                });
            }
            TrafficSigns = TrafficSigns.map(function (val) {
                return { signType: val.signType, longitude: val.longitude, latitude: val.latitude }
            });
            return res.json(TrafficSigns);
        });
    },

    show: function (req, res) {
        let id = req.params.id;
        TrafficSignModel.findOne({_id: id}, function (err, TrafficSign) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting TrafficSign.',
                    error: err
                });
            }
            if (!TrafficSign) {
                return res.status(404).json({
                    message: 'No such TrafficSign'
                });
            }
            return res.json(TrafficSign);
        });
    },

    create: function (req, res) {
        let TrafficSign = new TrafficSignModel({
			signType : req.body.signType,
			longitude : req.body.longitude,
			latitude : req.body.latitude,
			date : req.body.date

        });

        TrafficSign.save(function (err, TrafficSign) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating TrafficSign',
                    error: err
                });
            }
            return res.status(201).json(TrafficSign);
        });
    },

    update: function (req, res) {
        let id = req.params.id;
        TrafficSignModel.findOne({_id: id}, function (err, TrafficSign) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting TrafficSign',
                    error: err
                });
            }
            if (!TrafficSign) {
                return res.status(404).json({
                    message: 'No such TrafficSign'
                });
            }

            TrafficSign.signType = req.body.signType ? req.body.signType : TrafficSign.signType;
			TrafficSign.longitude = req.body.longitude ? req.body.longitude : TrafficSign.longitude;
			TrafficSign.latitude = req.body.latitude ? req.body.latitude : TrafficSign.latitude;
			TrafficSign.date = req.body.date ? req.body.date : TrafficSign.date;
			
            TrafficSign.save(function (err, TrafficSign) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating TrafficSign.',
                        error: err
                    });
                }

                return res.json(TrafficSign);
            });
        });
    },

    remove: function (req, res) {
        let id = req.params.id;
        TrafficSignModel.findByIdAndRemove(id, function (err, TrafficSign) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the TrafficSign.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    addTestData: function(req, res) {
        for (let i = 0; i < 100; i++) {
            let TrafficSign = new TrafficSignModel({
                signType : "STOP",
                longitude : 0.0,
                latitude : 0.0,
                date : Date.now()
            });

            TrafficSign.longitude = Math.random() * (16.6 - 13.3) + 13.3;
            TrafficSign.latitude = Math.random() * (46.9 - 45.4) + 45.4;

            TrafficSign.save(function (err, TrafficSign) {
                if (err) {
                    console.log(err)
                } else {
                    console.log("Inserted: " + i + ", " + TrafficSign);
                }
            });
        }

        return res.status(201).json("DONE");
    },

    addNewTS: function(req, res) {
        // TODO povezi notri skripte
        var pathToImg = "./public/images/" + req.file.filename;
        const pyScript = spawn('python', ['prepoznajZnak.py', 'PROD', 'PHOTO', pathToImg]);

        pyScript.stdout.on('data', (data) => {
            if (data != 0) {
                let TrafficSign = new TrafficSignModel({
                    signType : "STOP",
                    longitude : req.body.longitude,
                    latitude : req.body.latitude,
                    date : Date.now()
                });

                TrafficSign.save(function (err, TrafficSign) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error when creating TrafficSign',
                            error: err
                        });
                    }
                    return res.status(201).json("DONE");
                });
            }
        });

        pyScript.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
            return res.status(500).json("FAIL");
        });

        pyScript.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
        });
    }
};
