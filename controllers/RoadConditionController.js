let RoadConditionModel = require('../models/RoadConditionModel.js');
let spawn = require('child_process').spawn;
let fs = require('fs');

module.exports = {

    list: function (req, res) {
        RoadConditionModel.find(function (err, RoadConditions) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting RoadCondition.',
                    error: err
                });
            }
            RoadConditions = RoadConditions.map(function (val) {
                return { condition: val.condition, longitude: val.longitude, latitude: val.latitude };
            });
            return res.json(RoadConditions);
        });
    },

    show: function (req, res) {
        let id = req.params.id;
        RoadConditionModel.findOne({_id: id}, function (err, RoadCondition) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting RoadCondition.',
                    error: err
                });
            }
            if (!RoadCondition) {
                return res.status(404).json({
                    message: 'No such RoadCondition'
                });
            }
            return res.json(RoadCondition);
        });
    },

    create: function (req, res) {
        let RoadCondition = new RoadConditionModel({
			condition : req.body.condition,
			longitude : req.body.longitude,
			latitude : req.body.latitude,
			date : req.body.date
        });

        RoadCondition.save(function (err, RoadCondition) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating RoadCondition',
                    error: err
                });
            }
            return res.status(201).json(RoadCondition);
        });
    },

    update: function (req, res) {
        let id = req.params.id;
        RoadConditionModel.findOne({_id: id}, function (err, RoadCondition) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting RoadCondition',
                    error: err
                });
            }
            if (!RoadCondition) {
                return res.status(404).json({
                    message: 'No such RoadCondition'
                });
            }

            RoadCondition.condition = req.body.condition ? req.body.condition : RoadCondition.condition;
			RoadCondition.longitude = req.body.longitude ? req.body.longitude : RoadCondition.longitude;
			RoadCondition.latitude = req.body.latitude ? req.body.latitude : RoadCondition.latitude;
			RoadCondition.date = req.body.date ? req.body.date : RoadCondition.date;
			
            RoadCondition.save(function (err, RoadCondition) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating RoadCondition.',
                        error: err
                    });
                }

                return res.json(RoadCondition);
            });
        });
    },

    remove: function (req, res) {
        let id = req.params.id;
        RoadConditionModel.findByIdAndRemove(id, function (err, RoadCondition) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the RoadCondition.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    testingPyScript: function(req, res) {
        const pyTestScript = spawn('python', ['test.py', 'Helouuuuuuuuuuu']);

        pyTestScript.stdout.on('data', (data) => {
            console.log(data);
            var jsonOject = JSON.parse(data);
            console.log(jsonOject);
            res.send(data);
        })

        pyTestScript.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
            res.send("Ni uspelo : " + data)
        });

        pyTestScript.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
        });
    },

    addTestData: function(req, res) {
        for (let i = 0; i < 100; i++) {
            let RoadCondition = new RoadConditionModel({
                condition : 0,
                longitude : 0.0,
                latitude : 0.0,
                date : Date.now()
            });

            RoadCondition.longitude = Math.random() * (16.6 - 13.3) + 13.3;
            RoadCondition.latitude = Math.random() * (46.9 - 45.4) + 45.4;
            RoadCondition.condition = Math.floor(Math.random() * 2);

            RoadCondition.save(function (err, RoadCondition) {
                if (err) {
                    console.log(err)
                } else {
                    console.log("Inserted: " + i + ", " + RoadCondition);
                }
            });
        }

        return res.status(201).json("DONE");
    },

    addNewRC: function(req, res) {

        fs.writeFileSync(__dirname + '/../data.json', JSON.stringify(req.body));

        //const pyScript = spawn('python', ['prepoznavanjeStanja.py', 'PROD']);
	    const pyScript = spawn('mpirun', ['--np', '2', '--hosts', 'project1-server,project2-server', 'python', 'prepoznavanjeStanjaMPI.py', 'PROD']);

        pyScript.stdout.on('data', (data) => {
            var jsonOject = JSON.parse(data);
            console.log(jsonOject);

            let RoadCondition = new RoadConditionModel({
                condition : jsonOject.conditionStatus,
                longitude : jsonOject.longitude,
                latitude : jsonOject.latitude,
                date : Date.now()
            });

            if (jsonOject.latitude > 1 && jsonOject.longitude > 1) {
                RoadCondition.save(function (err, RoadCondition) {
                    if (err) {
                        console.log(err)
                        return res.status(500).send("Error: " + err);
                    } else {
                        console.log("Inserted: " + RoadCondition);
                        return res.status(200).send("Procesed");
                    }
                });
            } else {
                return res.status(200).send("Invalid cordinates");
            }
        });

        pyScript.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
            return res.status(500).send("Error : " + data)
        });

        pyScript.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
        });
    }
};
