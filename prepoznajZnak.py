# link do haar-cascades https://github.com/cfizette/road-sign-cascades
# uporaba haar-cascades https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_objdetect/py_face_detection/py_face_detection.html
# importi
import numpy as np
import cv2
import sys

# konstante
DEV = True
PHOTO = True
STOP_SIGN_CASCADE = cv2.CascadeClassifier('./haarcascade/stopsign_classifier.xml')

# globalne spremenljivke
img = None
gray = None
pathToImg = ""

# MAIN

if sys.argv[1] == "PROD":
    # produkcijski nacin, izklopimo dev in pot do slike pridobimo prek parametra
    DEV = False
    pathToImg = sys.argv[3]

if sys.argv[2] == "VIDEO":
    PHOTO = False
    pathToImg = "./photos/photo2.jpg"

if PHOTO:
    # branje slike
    img = cv2.imread(pathToImg) 
    # pretvorba slike v sivinsko
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # poiscemo s pomocjo haarcascade ali je v sliki kaksen stop znak
    signs = STOP_SIGN_CASCADE.detectMultiScale(gray, 1.3, 5)

    if DEV:
        print("LOKACIJA ZNAKA - ", signs)
        print("STEVILO ZNAKOV - ", len(signs))
        # ce je bil znak najdem narisemo okrog njega kvadrat
        for (x,y,w,h) in signs:
            img = cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)

        # prikaz fotografije
        cv2.imshow('Slika', img) 
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    else:
        # posljemo stevilo znakov nazaj na server (ce je 0 potem ni znaka, ce je kaj koli drugega potem je znak)
        print(len(signs))
        sys.stdout.flush()
else:
    # beremo video iz datoteke
    cap = cv2.VideoCapture('./videos/video1.mp4')

    # beremo frame po frame dokler video tece
    while(cap.isOpened()):
        ret, frame = cap.read()

        # trenutni frame pretvorimo v sivinsko sliko
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # na sivinski sliki preverimo ali je stop znak
        signs = STOP_SIGN_CASCADE.detectMultiScale(gray, 1.3, 5)

        # na trenutni frame narisemo pozicijo stop znaka
        for (x,y,w,h) in signs:
            frame = cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)

        # prikazemo trenutni frame
        cv2.imshow('Video', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # zapremo vsa okna
    cap.release()
    cv2.destroyAllWindows()
