# importi
import json
import sys

# konstante
DEV = True
SD_VALUE = 1.5
ROAD_IS_IN_GOOD_CONDITION = 0
ROAD_IS_IN_BAD_CONDITION = 1

# Strukture
class RoadCondition():
    conditionStatus = -1
    longitude = 0.0
    latitude = 0.0

# globalne spremenljivke
jsonData = ""

vibrationsData = list()
longitudeData = list()
latitudeData = list()

vibrationsAverage = 0
longitudeAverage = 0
latitudeAverage = 0
sdMaxValue = 0
sdMinValue = 0

# fukcije
# povprecje vseh vibracij
def averageOfVibrations(dataList):
    sumVibrations = 0

    for vibration in dataList:
        sumVibrations += vibration

    sumVibrations = sumVibrations / len(dataList)

    if DEV:
        print("Povprecje = ", sumVibrations)

    return sumVibrations

# povprecje lokacije (povprecna lokacija za obmocje meritve)
def averageOfLocation(longitudeList, latitudeList):
    sumLongitude = 0
    sumLatitude = 0

    for lon in longitudeList:
        sumLongitude += lon

    for lat in latitudeList:
        sumLatitude += lat

    sumLongitude = sumLongitude / len(longitudeList)
    sumLatitude = sumLatitude / len(latitudeList)

    return (longitudeList[-1], latitudeList[-1])

# funkcija ki odloci ali je opazovano obmocje dobro ali slabo cestisce
def decideRoadCondition(dataList, sdMaxValue, sdMinValue):
    badRoadCondition = 0
    goodRoadCondition = 0

    # ce je trenutna vrednost blizu povprecja vibracij jo
    # dolocimo kot dobro drugace pa kot slabo
    for i in dataList:
        if(i >= sdMinValue and i <= sdMaxValue):
            goodRoadCondition += 1
        else:
            badRoadCondition +=1
    if DEV:
        print("Slabo = ", badRoadCondition)
        print("Dobro = ", goodRoadCondition)

    # ce je slabih vec kot dobrih odlocimo da je opazovano obmocje
    # slabo cestisce drugace pa odlocimo da je dobro cestisce
    if(badRoadCondition > goodRoadCondition):
        return ROAD_IS_IN_BAD_CONDITION
    else:
        return ROAD_IS_IN_GOOD_CONDITION

# MAIN

# ce kot vhodni parameter dobimo PROD znacko izklopimo DEV nacin
if sys.argv[1] == "PROD":
    DEV = False

# branje JSON stringa
if DEV:
    print("Dev")
    with open('data.json') as json_file:
        jsonData = json.load(json_file)
else:
    with open('data.json') as json_file:
        jsonData = json.load(json_file)

# json objekt pretvorimo v tri liste podatkov
for obj in jsonData:
    vibrationsData.append(obj["z"])
    latitudeData.append(obj["latitude"])
    longitudeData.append(obj["longitude"])

# izracun povprecja lokacije in vibracij
vibrationsAverage = averageOfVibrations(vibrationsData)
locationAverage = averageOfLocation(longitudeData, latitudeData)

longitudeAverage = locationAverage[0]
latitudeAverage = locationAverage[1]

# minimalna in maksimalna meja do kamor so vibracije spremenljive kot dobre
sdMinValue = vibrationsAverage - SD_VALUE
sdMaxValue = vibrationsAverage + SD_VALUE

if DEV:
    print("MAX = ", sdMaxValue)
    print("MIN = ", sdMinValue)

    if(decideRoadCondition(vibrationsData, sdMaxValue, sdMinValue) == ROAD_IS_IN_GOOD_CONDITION):
        print("STANJE = Dobro")
    else:
        print("STANJE = Slabo")

    print("LONGITUDE = ", longitudeAverage)
    print("LATITUDE = ", latitudeAverage)
else:
    obj = RoadCondition()
    obj.conditionStatus = decideRoadCondition(vibrationsData, sdMaxValue, sdMinValue)
    obj.longitude = longitudeAverage
    obj.latitude = latitudeAverage

    outputString = json.dumps(obj.__dict__)

    print(outputString)
    sys.stdout.flush()
