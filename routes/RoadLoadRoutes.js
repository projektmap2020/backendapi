let express = require('express');
let router = express.Router();
let RoadLoadController = require('../controllers/RoadLoadController.js');

// router za endpointe povezani z RoadLoad
router.get('/', RoadLoadController.list);

router.get('/getCompresedData', RoadLoadController.sendCompressedData);

router.get('/addTestData', RoadLoadController.addTestData);

router.get('/roadAndYear/:roadName/:year', RoadLoadController.getDataForRoadAndYear);

router.get('/:id', RoadLoadController.show);

router.post('/', RoadLoadController.create);

router.put('/:id', RoadLoadController.update);

router.delete('/:id', RoadLoadController.remove);

module.exports = router;
