let express = require('express');
let router = express.Router();
let RoadConditionController = require('../controllers/RoadConditionController.js');

// router za endpointe povezani z RoadCondition
router.get('/', RoadConditionController.list);

router.get('/test', RoadConditionController.testingPyScript);

router.get('/addTestData', RoadConditionController.addTestData);

router.get('/:id', RoadConditionController.show);

router.post('/', RoadConditionController.create);

router.post('/addNew', RoadConditionController.addNewRC);

router.put('/:id', RoadConditionController.update);

router.delete('/:id', RoadConditionController.remove);

module.exports = router;
