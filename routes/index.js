let express = require('express');
let router = express.Router();

// router za osnovno stran z dokumentacijo
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

module.exports = router;
