let express = require('express');
let router = express.Router();
let TrafficSignController = require('../controllers/TrafficSignController.js');

let multer = require('multer');
let upload = multer({ dest: 'public/images/' });

// router za endpointe povezani z TrafficSign
router.get('/', TrafficSignController.list);

router.get('/addTestData', TrafficSignController.addTestData);

router.get('/:id', TrafficSignController.show);

router.post('/', TrafficSignController.create);

router.post('/addNew', upload.single('image'), TrafficSignController.addNewTS);

router.put('/:id', TrafficSignController.update);

router.delete('/:id', TrafficSignController.remove);

module.exports = router;
