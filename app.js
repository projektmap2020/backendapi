let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let cors = require('cors');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');

let indexRouter = require('./routes/index');
let roadConditionRouter = require('./routes/RoadConditionRoutes');
let roadLoadRouter = require('./routes/RoadLoadRoutes');
let trafficSignRouter = require('./routes/TrafficSignRoutes');

let app = express();

// nastavitve mongoDB baze
mongoose.connect('mongodb://localhost/road-statistics');

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => { console.log('Connect success!') });

// nastavitve view engina
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());    // potrebujemo cors
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

// natavimo routerje
app.use('/', indexRouter);
app.use('/api/rc', roadConditionRouter);
app.use('/api/rl', roadLoadRouter);
app.use('/api/ts', trafficSignRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
